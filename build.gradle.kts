import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.4.10"
    application
    id("com.github.johnrengelman.shadow") version "5.0.0"
}

group = "com.ultraone"
version = "alpha"

repositories {
    jcenter()
    mavenCentral()
    maven { url = uri("https://dl.bintray.com/kotlin/kotlinx") }
    maven { url = uri("https://dl.bintray.com/kotlin/ktor") }
}
val ktor_v = "1.4.1"
dependencies {
    testImplementation(kotlin("test-junit"))
    implementation("io.ktor:ktor-server-netty:1.4.0")
    implementation("io.ktor:ktor-html-builder:1.4.0")
    implementation("io.ktor:ktor-websockets:$ktor_v")
    implementation("io.ktor:ktor-client-websockets:$ktor_v")
    implementation("io.ktor:ktor-client-cio:$ktor_v")
    implementation("org.jetbrains.kotlinx:kotlinx-html-jvm:0.7.2")
}

tasks.test {
    useJUnit()
}

tasks.withType<KotlinCompile>() {
    kotlinOptions.jvmTarget = "1.8"
}

application {
   mainClassName = "ClientKt"
}
tasks.named<com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar>("shadowJar") {
    // Defaults to rootProjectDir/build/libs/${project.name}.jar
    // destinationDir = "/Custom/Directory/${rootProject.name}/${project.name}"
    manifest {
        attributes(mapOf("Main-Class" to "ServerKt"))
    }

}