job("Build and run tests") {
    container("openjdk:11") {
        resources {
            memory = 768
        }

        kotlinScript { api ->
            // here goes complex logic
            api.gradlew("build")
        }
    }
}
