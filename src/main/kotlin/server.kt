import io.ktor.application.*

import io.ktor.http.cio.websocket.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.websocket.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*
import java.util.concurrent.atomic.AtomicInteger
import kotlin.collections.LinkedHashSet
fun main(){
    println("Starting..")
    /**
     * Main function
*/  val mPort = System.getProperty("PORT")?.toInt() ?: 23567
    CoroutineScope(Dispatchers.Unconfined).launch {
        embeddedServer(Netty, port = mPort){
            doPost()
        }.start(true)
    }

}
class Connection(val session: DefaultWebSocketSession){
    companion object {
        var lastId = AtomicInteger(0)
    }
    val name = "user${lastId.getAndIncrement()}"
}
fun Application.doPost() {
    install(WebSockets)
    val connections = Collections.synchronizedSet<Connection?>(LinkedHashSet())
    routing {
        webSocket("/chats-room") {
            send("Online!")
            val thisConnection = Connection(this)
            connections += thisConnection
            try {
                send(" there are ${connections.count()}")
                for (frame in incoming) {
                    frame as? Frame.Text ?: continue
                    val receiveText = frame.readText()
                    val textWithUserName = "{${thisConnection.name}}: $receiveText"
                    connections.forEach {
                        it.session.send(textWithUserName)
                    }
                }
            }catch (ex: Exception){
                println("${ex.localizedMessage}")
            }finally {
                println("$thisConnection has been removed")
                connections -= thisConnection
            }
        }
        get("/test"){
            this.call.respondText("Done!")
        }
    }
}