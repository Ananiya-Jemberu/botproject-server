import io.ktor.client.*
import io.ktor.client.features.websocket.*
import io.ktor.http.*
import io.ktor.http.cio.websocket.*
import io.ktor.util.*
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlin.io.readText

@KtorExperimentalAPI
fun main(){
    val client = HttpClient {
        install(WebSockets)
    }
    runBlocking {
        client.webSocket(method = HttpMethod.Get, host = "https://bot-project-server.herokuapp.com", path = "/chats-room") {
            val msgOutPutRoutine = launch {outputMessages()}
            val msgInputRoutine = launch {inputMessages()}
            msgInputRoutine.join()
            msgOutPutRoutine.cancelAndJoin()
        }

    }
}
suspend fun DefaultClientWebSocketSession.inputMessages(){
    while (true){
        val message = readLine() ?: " "
        if(message  == "quit") return
        try {
            send(message)
        } catch (ex: Exception){
            println("An error X| ${ex.localizedMessage}")
            return
        }
    }
}
suspend fun DefaultClientWebSocketSession.outputMessages(){
    try {
        for (message in incoming){
            message as? Frame.Text ?: continue
            println(message.readText())
        }
    } catch (ex: Exception){
        println("X| an error occured ${ex.localizedMessage}")
    }
}